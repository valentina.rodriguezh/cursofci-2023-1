import math
import numpy as np
import matplotlib.pyplot as plt 


class particulac():

    def __init__(self,elec,m,Ek,b,x_0,y_0,theta,z_0,t,beta):
       



#parametros

        self.elec=elec #carga electron
        self.m=m #masa electron
        self.b=b #campo magnetico en z
        self.Ek=Ek #energia cinetica
        self.x_0=x_0 #posicion inicial en x
        self.y_0=y_0 #posicion inicial en y
        self.z_0=z_0 #posicion inicial en z
        self.theta=theta #angulo de la trayectoria
        self.t=t #tiempo
        self.beta=beta #angulo del plano xy
#posicion en x
    def posX(self):
        pos_x= self.x_0+(((np.sqrt(2*self.Ek/self.m))*np.sin(self.theta)*np.cos(self.beta))/(self.elec*self.b/self.m))*np.cos((self.elec*self.b/self.m)*self.t)
        return pos_x
#posicion en y
    def posY(self):
        pos_y= self.y_0+(((np.sqrt(2*self.Ek/self.m))*np.sin(self.theta))/(self.elec*self.b/self.m)*np.sin(self.beta))*np.sin((self.elec*self.b/self.m)*self.t)
        return pos_y
#posicion en z  
    def posZ(self):
        pos_z= self.z_0+((np.sqrt(2*self.Ek/self.m))*np.sin(self.theta)*self.t)
        return pos_z
#graficando
    def grafica(self):
        fig = plt.figure()
        ax = plt.axes(projection='3d')
        ax.plot3D(self.posX(), self.posY(), self.posZ())
        plt.savefig("grafica.png")
        plt.show()