
import numpy as np
from tarea21 import Integral

if __name__ =='__main__':

  a=0
  b=np.pi
  N=1000
  solution=Integral(a,b,N)
  solution.figMp2()
  

  print('Aproximación de la integral de f(x) con el método de Monte Carlo:', solution.valores_integral()[-1])
  print('Valor exacto de la integral de f(x) utilizando scipy.integrate.quad:', solution.integral_real())


